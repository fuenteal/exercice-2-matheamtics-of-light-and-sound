# Exercice 2 Matheamtics of Light and Sound

Exercise 2 of the sample exam: Write a program to plot the motion over time of set discrete particles in a
transverse acoustic wave. Use a closed-form solution of the wave equation